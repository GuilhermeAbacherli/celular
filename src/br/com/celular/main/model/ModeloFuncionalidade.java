package br.com.celular.main.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Guilherme Pedrozo Abacherli
 */
@Entity
@Table(name = "modelo_funcionalidade")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "ModeloFuncionalidade.findAll", query = "SELECT m FROM ModeloFuncionalidade m"),
		@NamedQuery(name = "ModeloFuncionalidade.findByModeloFuncionalidadeCodigo", query = "SELECT m FROM ModeloFuncionalidade m WHERE m.modeloFuncionalidadeCodigo = :modeloFuncionalidadeCodigo") })
public class ModeloFuncionalidade implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "modelo_funcionalidade_codigo")
	private Integer modeloFuncionalidadeCodigo;
	@JoinColumn(name = "modelo_funcionalidade_funcionalidade", referencedColumnName = "funcionalidade_codigo")
	@ManyToOne
	private Funcionalidade modeloFuncionalidadeFuncionalidade;
	@JoinColumn(name = "modelo_funcionalidade_modelo", referencedColumnName = "modelo_codigo")
	@ManyToOne
	private Modelo modeloFuncionalidadeModelo;

	public ModeloFuncionalidade() {
	}

	public ModeloFuncionalidade(Integer modeloFuncionalidadeCodigo) {
		this.modeloFuncionalidadeCodigo = modeloFuncionalidadeCodigo;
	}

	public Integer getModeloFuncionalidadeCodigo() {
		return modeloFuncionalidadeCodigo;
	}

	public void setModeloFuncionalidadeCodigo(Integer modeloFuncionalidadeCodigo) {
		this.modeloFuncionalidadeCodigo = modeloFuncionalidadeCodigo;
	}

	public Funcionalidade getModeloFuncionalidadeFuncionalidade() {
		return modeloFuncionalidadeFuncionalidade;
	}

	public void setModeloFuncionalidadeFuncionalidade(Funcionalidade modeloFuncionalidadeFuncionalidade) {
		this.modeloFuncionalidadeFuncionalidade = modeloFuncionalidadeFuncionalidade;
	}

	public Modelo getModeloFuncionalidadeModelo() {
		return modeloFuncionalidadeModelo;
	}

	public void setModeloFuncionalidadeModelo(Modelo modeloFuncionalidadeModelo) {
		this.modeloFuncionalidadeModelo = modeloFuncionalidadeModelo;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (modeloFuncionalidadeCodigo != null ? modeloFuncionalidadeCodigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof ModeloFuncionalidade)) {
			return false;
		}
		ModeloFuncionalidade other = (ModeloFuncionalidade) object;
		if ((this.modeloFuncionalidadeCodigo == null && other.modeloFuncionalidadeCodigo != null)
				|| (this.modeloFuncionalidadeCodigo != null
						&& !this.modeloFuncionalidadeCodigo.equals(other.modeloFuncionalidadeCodigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "br.com.lojascem.teste.entity.ModeloFuncionalidade[ modeloFuncionalidadeCodigo="
				+ modeloFuncionalidadeCodigo + " ]";
	}
}