package br.com.celular.main.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author Guilherme Pedrozo Abacherli
 */
@Entity
@Table(name = "fabricante")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Fabricante.findAll", query = "SELECT f FROM Fabricante f"),
		@NamedQuery(name = "Fabricante.findByFabricanteCodigo", query = "SELECT f FROM Fabricante f WHERE f.fabricanteCodigo = :fabricanteCodigo"),
		@NamedQuery(name = "Fabricante.findByFabricanteNome", query = "SELECT f FROM Fabricante f WHERE f.fabricanteNome = :fabricanteNome"),
		@NamedQuery(name = "Fabricante.findByFabricantePaisorigem", query = "SELECT f FROM Fabricante f WHERE f.fabricantePaisorigem = :fabricantePaisorigem") })
public class Fabricante implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "fabricante_codigo")
	private Integer fabricanteCodigo;
	@Size(max = 100)
	@Column(name = "fabricante_nome")
	private String fabricanteNome;
	@Size(max = 100)
	@Column(name = "fabricante_paisorigem")
	private String fabricantePaisorigem;
	@OneToMany(mappedBy = "modeloFabricante")
	private Collection<Modelo> modeloCollection;

	public Fabricante() {
	}

	public Fabricante(Integer fabricanteCodigo) {
		this.fabricanteCodigo = fabricanteCodigo;
	}

	public Integer getFabricanteCodigo() {
		return fabricanteCodigo;
	}

	public void setFabricanteCodigo(Integer fabricanteCodigo) {
		this.fabricanteCodigo = fabricanteCodigo;
	}

	public String getFabricanteNome() {
		return fabricanteNome;
	}

	public void setFabricanteNome(String fabricanteNome) {
		this.fabricanteNome = fabricanteNome;
	}

	public String getFabricantePaisorigem() {
		return fabricantePaisorigem;
	}

	public void setFabricantePaisorigem(String fabricantePaisorigem) {
		this.fabricantePaisorigem = fabricantePaisorigem;
	}

	@XmlTransient
	public Collection<Modelo> getModeloCollection() {
		return modeloCollection;
	}

	public void setModeloCollection(Collection<Modelo> modeloCollection) {
		this.modeloCollection = modeloCollection;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (fabricanteCodigo != null ? fabricanteCodigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Fabricante)) {
			return false;
		}
		Fabricante other = (Fabricante) object;
		if ((this.fabricanteCodigo == null && other.fabricanteCodigo != null)
				|| (this.fabricanteCodigo != null && !this.fabricanteCodigo.equals(other.fabricanteCodigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "br.com.lojascem.teste.entity.Fabricante[ fabricanteCodigo=" + fabricanteCodigo + " ]";
	}
}