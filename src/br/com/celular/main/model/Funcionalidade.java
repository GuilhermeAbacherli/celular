package br.com.celular.main.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author Guilherme Pedrozo Abacherli
 */
@Entity
@Table(name = "funcionalidade")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Funcionalidade.findAll", query = "SELECT f FROM Funcionalidade f"),
		@NamedQuery(name = "Funcionalidade.findByFuncionalidadeCodigo", query = "SELECT f FROM Funcionalidade f WHERE f.funcionalidadeCodigo = :funcionalidadeCodigo"),
		@NamedQuery(name = "Funcionalidade.findByFuncionalidadeNome", query = "SELECT f FROM Funcionalidade f WHERE f.funcionalidadeNome = :funcionalidadeNome") })
public class Funcionalidade implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "funcionalidade_codigo")
	private Integer funcionalidadeCodigo;
	@Size(max = 100)
	@Column(name = "funcionalidade_nome")
	private String funcionalidadeNome;
	@OneToMany(mappedBy = "modeloFuncionalidadeFuncionalidade")
	private Collection<ModeloFuncionalidade> modeloFuncionalidadeCollection;

	public Funcionalidade() {
	}

	public Funcionalidade(Integer funcionalidadeCodigo) {
		this.funcionalidadeCodigo = funcionalidadeCodigo;
	}

	public Integer getFuncionalidadeCodigo() {
		return funcionalidadeCodigo;
	}

	public void setFuncionalidadeCodigo(Integer funcionalidadeCodigo) {
		this.funcionalidadeCodigo = funcionalidadeCodigo;
	}

	public String getFuncionalidadeNome() {
		return funcionalidadeNome;
	}

	public void setFuncionalidadeNome(String funcionalidadeNome) {
		this.funcionalidadeNome = funcionalidadeNome;
	}

	@XmlTransient
	public Collection<ModeloFuncionalidade> getModeloFuncionalidadeCollection() {
		return modeloFuncionalidadeCollection;
	}

	public void setModeloFuncionalidadeCollection(Collection<ModeloFuncionalidade> modeloFuncionalidadeCollection) {
		this.modeloFuncionalidadeCollection = modeloFuncionalidadeCollection;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (funcionalidadeCodigo != null ? funcionalidadeCodigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Funcionalidade)) {
			return false;
		}
		Funcionalidade other = (Funcionalidade) object;
		if ((this.funcionalidadeCodigo == null && other.funcionalidadeCodigo != null)
				|| (this.funcionalidadeCodigo != null
						&& !this.funcionalidadeCodigo.equals(other.funcionalidadeCodigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "br.com.lojascem.teste.entity.Funcionalidade[ funcionalidadeCodigo=" + funcionalidadeCodigo + " ]";
	}
}