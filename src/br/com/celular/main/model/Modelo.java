package br.com.celular.main.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author Guilherme Pedrozo Abacherli
 */
@Entity
@Table(name = "modelo")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Modelo.findAll", query = "SELECT m FROM Modelo m"),
		@NamedQuery(name = "Modelo.findByModeloCodigo", query = "SELECT m FROM Modelo m WHERE m.modeloCodigo = :modeloCodigo"),
		@NamedQuery(name = "Modelo.findByModeloModelo", query = "SELECT m FROM Modelo m WHERE m.modeloModelo = :modeloModelo"),
		@NamedQuery(name = "Modelo.findByModeloFlash", query = "SELECT m FROM Modelo m WHERE m.modeloFlash = :modeloFlash"),
		@NamedQuery(name = "Modelo.findByModeloImagem", query = "SELECT m FROM Modelo m WHERE m.modeloImagem = :modeloImagem"),
		@NamedQuery(name = "Modelo.findByModeloTamanhotela", query = "SELECT m FROM Modelo m WHERE m.modeloTamanhotela = :modeloTamanhotela") })
public class Modelo implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "modelo_codigo")
	private Integer modeloCodigo;
	@Size(max = 100)
	@Column(name = "modelo_modelo")
	private String modeloModelo;
	@Column(name = "modelo_flash")
	private Boolean modeloFlash;
	@Size(max = 255)
	@Column(name = "modelo_imagem")
	private String modeloImagem;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
	// consider using these annotations to enforce field validation
	@Column(name = "modelo_tamanhotela")
	private Double modeloTamanhotela;
	@JoinColumn(name = "modelo_fabricante", referencedColumnName = "fabricante_codigo")
	@ManyToOne
	private Fabricante modeloFabricante;
	@OneToMany(mappedBy = "modeloFuncionalidadeModelo")
	private Collection<ModeloFuncionalidade> modeloFuncionalidadeCollection;

	public Modelo() {
	}

	public Modelo(Integer modeloCodigo) {
		this.modeloCodigo = modeloCodigo;
	}

	public Integer getModeloCodigo() {
		return modeloCodigo;
	}

	public void setModeloCodigo(Integer modeloCodigo) {
		this.modeloCodigo = modeloCodigo;
	}

	public String getModeloModelo() {
		return modeloModelo;
	}

	public void setModeloModelo(String modeloModelo) {
		this.modeloModelo = modeloModelo;
	}

	public Boolean getModeloFlash() {
		return modeloFlash;
	}

	public void setModeloFlash(Boolean modeloFlash) {
		this.modeloFlash = modeloFlash;
	}

	public String getModeloImagem() {
		return modeloImagem;
	}

	public void setModeloImagem(String modeloImagem) {
		this.modeloImagem = modeloImagem;
	}

	public Double getModeloTamanhotela() {
		return modeloTamanhotela;
	}

	public void setModeloTamanhotela(Double modeloTamanhotela) {
		this.modeloTamanhotela = modeloTamanhotela;
	}

	public Fabricante getModeloFabricante() {
		return modeloFabricante;
	}

	public void setModeloFabricante(Fabricante modeloFabricante) {
		this.modeloFabricante = modeloFabricante;
	}

	@XmlTransient
	public Collection<ModeloFuncionalidade> getModeloFuncionalidadeCollection() {
		return modeloFuncionalidadeCollection;
	}

	public void setModeloFuncionalidadeCollection(Collection<ModeloFuncionalidade> modeloFuncionalidadeCollection) {
		this.modeloFuncionalidadeCollection = modeloFuncionalidadeCollection;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (modeloCodigo != null ? modeloCodigo.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Modelo)) {
			return false;
		}
		Modelo other = (Modelo) object;
		if ((this.modeloCodigo == null && other.modeloCodigo != null)
				|| (this.modeloCodigo != null && !this.modeloCodigo.equals(other.modeloCodigo))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "br.com.lojascem.teste.entity.Modelo[ modeloCodigo=" + modeloCodigo + " ]";
	}
}