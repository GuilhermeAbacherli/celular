package br.com.celular.main.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.celular.main.dao.FabricanteDAO;
import br.com.celular.main.model.Fabricante;

@Named(value = "fabricanteBean")
@ViewScoped
public class FabricanteBean {

	@Inject
	private FabricanteDAO fabricanteDao;

	private Fabricante selectedFabricante;
	private List<Fabricante> listaFabricantes;

	@PostConstruct
	public void init() {
		listarFabricantes();
	}

	public boolean insertFabricante() {
		try {
			Fabricante novoFabricante = new Fabricante();
			novoFabricante.setFabricanteNome(selectedFabricante.getFabricanteNome());
			novoFabricante.setFabricantePaisorigem(selectedFabricante.getFabricantePaisorigem());
			fabricanteDao.insertFabricante(novoFabricante);
			return true;

		} catch (Exception e) {
			System.out.println("Erro no FabricanteBean ao inserir novo fabricante!");
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	public boolean deleteFabricante() {
		try {
			return fabricanteDao.deleteFabricante(selectedFabricante);

		} catch (Exception e) {
			System.out.println("Erro no FabricanteBean ao remover fabricante!");
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	public boolean updateFabricante() {
		try {
			return fabricanteDao.updateFabricante(selectedFabricante);

		} catch (Exception e) {
			System.out.println("Erro no FabricanteBean ao atualizar fabricante!");
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	public boolean listarFabricantes() {
		try {
			listaFabricantes = fabricanteDao.listarFabricantes();
			return true;

		} catch (Exception e) {
			System.out.println("Erro no FabricanteBean ao listar fabricantes!");
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	public Fabricante getSelectedFabricante() {
		return selectedFabricante;
	}

	public void setSelectedFabricante(Fabricante selectedFabricante) {
		this.selectedFabricante = selectedFabricante;
	}

	public List<Fabricante> getListaFabricantes() {
		return listaFabricantes;
	}

	public void setListaFabricantes(List<Fabricante> listaFabricantes) {
		this.listaFabricantes = listaFabricantes;
	}
}