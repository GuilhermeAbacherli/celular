package br.com.celular.main.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

public abstract class DAO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * EntityManager para interagir com a JPA.
	 */
	@PersistenceContext(unitName = "CelularPU")
	EntityManager manager;

	// M�todos CRUD - Create, Read, Update, Delete.

	/**
	 * Salva (persist) uma nova entidade.
	 *
	 * @param        <T> Tipo da entidade.
	 * @param entity Entidade (objeto).
	 * @return Mensagem (String) de sucesso ou erro da opera��o de persist.
	 */
	@Transactional
	public <T> String salvar(T entity) {
		try {
			manager.persist(entity);
			return "Os dados foram salvos com sucesso!";
		} catch (Exception e) {
			System.out.println("ERRO AO CADASTRAR - DAO.java\n" + e.getMessage());
			return "Erro ao salvar os dados!";
		}
	}

	/**
	 * L� (find) uma entidade com base na chave prim�ria.
	 *
	 * @param       <T> Tipo da entidade.
	 * @param key   Chave prim�ria.
	 * @param clazz Classe do objeto.
	 * @return Retorna o objeto completo.
	 */
	public <T> T ler(Object key, Class<T> clazz) {
		return manager.find(clazz, key);
	}

	/**
	 * Atualiza (merge) uma entidade existente.
	 *
	 * @param        <T> Tipo da entidade.
	 * @param entity Entidade (objeto).
	 * @return Mensagem (String) de sucesso ou erro da opera��o de merge.
	 */
	public <T> String atualizar(T entity) {
		try {
			manager.merge(entity);
			return "Os dados foram atualizados com sucesso!";
		} catch (Exception e) {
			return "Erro ao atualizar os dados!";
		}
	}

	/**
	 * Exclui (remove) uma entidade existente.
	 *
	 * @param        <T> Tipo da entidade.
	 * @param entity Entidade (objeto).
	 * @return Mensagem (String) de sucesso ou erro da opera��o de remove.
	 */
	public <T> String excluir(T entity) {
		try {
			manager.remove(entity);
			return "Os dados foram exclu�dos com sucesso!";
		} catch (Exception e) {
			return "Erro ao excluir os dados!";
		}
	}

	/**
	 * Cria um objeto Query para uma query JPQL.
	 *
	 * @param query
	 * @return Manager com a query criada.
	 */
	protected Query criarQuery(String query) {
		return manager.createQuery(query);
	}
}