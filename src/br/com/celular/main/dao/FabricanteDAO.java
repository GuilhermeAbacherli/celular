package br.com.celular.main.dao;

import br.com.celular.main.model.Fabricante;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;

@SuppressWarnings("serial")
@RequestScoped
public class FabricanteDAO extends DAO {

	@Transactional
	public boolean insertFabricante(Fabricante fabricante) {
		try {
			manager.persist(fabricante);
			return true;

		} catch (Exception e) {
			System.out.println("Erro no FabricanteDAO ao inserir fabricante " + fabricante.getFabricanteNome());
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	@Transactional
	public boolean deleteFabricante(Fabricante fabricante) {
		try {
			manager.remove(fabricante);
			return true;

		} catch (Exception e) {
			System.out.println("Erro no FabricanteDAO ao remover fabricante " + fabricante.getFabricanteNome());
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	@Transactional
	public boolean updateFabricante(Fabricante fabricante) {
		try {
			manager.merge(fabricante);
			return true;

		} catch (Exception e) {
			System.out.println("Erro no FabricanteDAO ao atualizar fabricante " + fabricante.getFabricanteNome());
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Fabricante> listarFabricantes() {
		try {
			List<Fabricante> listaFabricantes = new ArrayList<Fabricante>();
			listaFabricantes = manager.createQuery("SELECT f FROM Fabricante f").getResultList();
			return listaFabricantes;

		} catch (Exception e) {
			System.out.println("Erro no FabricanteDAO ao listar fabricantes!");
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return null;
		}
	}
}