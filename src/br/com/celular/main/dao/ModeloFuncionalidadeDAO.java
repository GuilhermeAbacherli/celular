package br.com.celular.main.dao;

import br.com.celular.main.model.ModeloFuncionalidade;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;

@SuppressWarnings("serial")
@RequestScoped
public class ModeloFuncionalidadeDAO extends DAO {

	@Transactional
	public boolean insertModeloFuncionalidade(ModeloFuncionalidade ModeloFuncionalidade) {
		try {
			manager.persist(ModeloFuncionalidade);
			return true;

		} catch (Exception e) {
			System.out.println("Erro no ModeloFuncionalidadeDAO ao inserir ModeloFuncionalidade "
					+ ModeloFuncionalidade.getModeloFuncionalidadeCodigo());
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	@Transactional
	public boolean deleteModeloFuncionalidade(ModeloFuncionalidade ModeloFuncionalidade) {
		try {
			manager.remove(ModeloFuncionalidade);
			return true;

		} catch (Exception e) {
			System.out.println("Erro no ModeloFuncionalidadeDAO ao remover ModeloFuncionalidade "
					+ ModeloFuncionalidade.getModeloFuncionalidadeCodigo());
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	@Transactional
	public boolean updateModeloFuncionalidade(ModeloFuncionalidade ModeloFuncionalidade) {
		try {
			manager.merge(ModeloFuncionalidade);
			return true;

		} catch (Exception e) {
			System.out.println("Erro no ModeloFuncionalidadeDAO ao atualizar ModeloFuncionalidade "
					+ ModeloFuncionalidade.getModeloFuncionalidadeCodigo());
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<ModeloFuncionalidade> listarModeloFuncionalidades() {
		try {
			List<ModeloFuncionalidade> listaModeloFuncionalidades = new ArrayList<ModeloFuncionalidade>();
			listaModeloFuncionalidades = manager.createQuery("SELECT mf FROM ModeloFuncionalidade mf").getResultList();
			return listaModeloFuncionalidades;

		} catch (Exception e) {
			System.out.println("Erro no ModeloFuncionalidadeDAO ao listar ModeloFuncionalidades!");
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return null;
		}
	}
}