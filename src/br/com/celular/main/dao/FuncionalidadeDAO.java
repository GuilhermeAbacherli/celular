package br.com.celular.main.dao;

import br.com.celular.main.model.Funcionalidade;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;

@SuppressWarnings("serial")
@RequestScoped
public class FuncionalidadeDAO extends DAO {

	@Transactional
	public boolean insertFuncionalidade(Funcionalidade funcionalidade) {
		try {
			manager.persist(funcionalidade);
			return true;

		} catch (Exception e) {
			System.out.println(
					"Erro no FuncionalidadeDAO ao inserir funcionalidade " + funcionalidade.getFuncionalidadeNome());
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	@Transactional
	public boolean deleteFuncionalidade(Funcionalidade funcionalidade) {
		try {
			manager.remove(funcionalidade);
			return true;

		} catch (Exception e) {
			System.out.println(
					"Erro no FuncionalidadeDAO ao remover funcionalidade " + funcionalidade.getFuncionalidadeNome());
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	@Transactional
	public boolean updateFuncionalidade(Funcionalidade funcionalidade) {
		try {
			manager.merge(funcionalidade);
			return true;

		} catch (Exception e) {
			System.out.println(
					"Erro no FuncionalidadeDAO ao atualizar funcionalidade " + funcionalidade.getFuncionalidadeNome());
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Funcionalidade> listarFuncionalidades() {
		try {
			List<Funcionalidade> listaFuncionalidades = new ArrayList<Funcionalidade>();
			listaFuncionalidades = manager.createQuery("SELECT f FROM Funcionalidade f").getResultList();
			return listaFuncionalidades;

		} catch (Exception e) {
			System.out.println("Erro no FuncionalidadeDAO ao listar funcionalidades!");
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return null;
		}
	}
}