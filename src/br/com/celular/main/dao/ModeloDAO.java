package br.com.celular.main.dao;

import br.com.celular.main.model.Modelo;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;

@SuppressWarnings("serial")
@RequestScoped
public class ModeloDAO extends DAO {

	@Transactional
	public boolean insertModelo(Modelo modelo) {
		try {
			manager.persist(modelo);
			return true;

		} catch (Exception e) {
			System.out.println("Erro no ModeloDAO ao inserir Modelo " + modelo.getModeloModelo());
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	@Transactional
	public boolean deleteModelo(Modelo modelo) {
		try {
			manager.remove(modelo);
			return true;

		} catch (Exception e) {
			System.out.println("Erro no ModeloDAO ao remover Modelo " + modelo.getModeloModelo());
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	@Transactional
	public boolean updateModelo(Modelo modelo) {
		try {
			manager.merge(modelo);
			return true;

		} catch (Exception e) {
			System.out.println("Erro no ModeloDAO ao atualizar modelo " + modelo.getModeloModelo());
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Modelo> listarModelos() {
		try {
			List<Modelo> listaModelos = new ArrayList<Modelo>();
			listaModelos = manager.createQuery("SELECT m FROM Modelo m").getResultList();
			return listaModelos;

		} catch (Exception e) {
			System.out.println("Erro no ModeloDAO ao listar modelos!");
			System.out.println("Mensagem de erro:\n" + e.getMessage());
			return null;
		}
	}
}