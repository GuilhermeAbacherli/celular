-- Seleciona a DATABASE MASTER para executar os comandos.
USE MASTER
	GO

-- Verifica se a DATABASE "Celular" j� existe,
-- se sim fecha todas as conex�es existentes e executa um DROP DATABASE na mesma para recriar.
WHILE EXISTS(select * from sys.databases where name='Celular')
		BEGIN
			DECLARE @SQL varchar(max)
			SELECT @SQL = COALESCE(@SQL,'') + 'Kill ' + Convert(varchar, SPId) + ';'
			FROM MASTER..SysProcesses
			WHERE DBId = DB_ID(N'Celular') AND SPId <> @@SPId
			EXEC(@SQL)
			DROP DATABASE [Celular]
		END
	GO

-- Cria a DATABASE "Celular".
CREATE DATABASE Celular
	GO

--Seleciona a DATABASE "Celular".
USE Celular
	GO

CREATE TABLE fabricante(
fabricante_codigo INT PRIMARY KEY IDENTITY(1,1),
fabricante_nome NVARCHAR(100),
fabricante_paisorigem NVARCHAR(100)
)

CREATE TABLE funcionalidade(
funcionalidade_codigo INT PRIMARY KEY IDENTITY(1,1),
funcionalidade_nome NVARCHAR(100)
)

CREATE TABLE modelo(
modelo_codigo INT PRIMARY KEY IDENTITY(1,1),
modelo_fabricante INT REFERENCES fabricante,
modelo_modelo NVARCHAR(100),
modelo_flash BIT,
modelo_imagem NVARCHAR(255),
modelo_tamanhotela FLOAT
)

CREATE TABLE modelo_funcionalidade(
modelo_funcionalidade_codigo INT PRIMARY KEY IDENTITY(1,1),
modelo_funcionalidade_modelo INT REFERENCES modelo,
modelo_funcionalidade_funcionalidade INT REFERENCES funcionalidade
)